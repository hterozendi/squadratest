package com.mycompany.app;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.net.URL;
import java.util.List;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class TaskControllerTest {

    @LocalServerPort
    private int port;

    private URL base;	
	
	private static final long ID = 1;
	private static final String NAME = "Firt Task";

    private TestRestTemplate template = new TestRestTemplate();
	
	@Autowired
	private TaskRepository taskRepository;
	

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
		Task task = new Task();
		task.setName(NAME);
		task.setId(ID);
		task = taskRepository.save(task);
    }

    @Test
    public void getTask() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        assertThat(response.getBody(), equalTo("Greetings from Spring Boot!"));
    }
	
	
	@Test
	public void testFindTaskByID() {
		 ResponseEntity<Task> response = template.getForEntity(base.toString()+"/tasks/"+ID,
                Task.class);
		 Task task_aux = response.getBody();
		 assertTrue(task_aux!=null);
	}

	@Test
	public void testFindTaskByName() {
		 ResponseEntity<Task> response = template.getForEntity(base.toString()+"/tasks/"+NAME,
                Task.class);
		 Task task_aux = response.getBody();
		 assertTrue(task_aux!=null);
	}

    @Test
	public void testDeleteTask()  throws Exception {
        template.delete(base.toString()+"/tasks/"+ID, Task.class);
	}

	@Test
	public void testPutTask()  throws Exception {
		Task task_aux = new Task();        
        task_aux.setName("Other Task");
        task_aux.setCompleted(true);
		task_aux.setWeight(3);
		
        template.put(base.toString()+"/tasks/"+ID, task_aux,Task.class);
	}


    @Test
    public void testPersistencTask() throws Exception {

        Task task_aux = new Task();        
        task_aux.setName("First Task");
        task_aux.setCompleted(true);
        task_aux.setWeight(5);
        
        ResponseEntity<Task> response = template.postForEntity(base.toString()+"/tasks", task_aux,
        Task.class);
        task_aux = response.getBody();
        assertTrue(response.getBody() != null);
    }
}