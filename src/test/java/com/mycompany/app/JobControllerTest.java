package com.mycompany.app;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.net.URL;
import java.util.List;

import org.junit.*;
import org.junit.runner.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = App.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
public class JobControllerTest {

    @LocalServerPort
    private int port;

    private URL base;	
	
	private static final long ID = 1;
	private static final String NAME = "Job_1";

    private TestRestTemplate template = new TestRestTemplate();
	
	@Autowired
	private JobRepository jobRepository;
	

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
		Job job = new Job();
		job.setName(NAME);
		job.setId(ID);
		job = jobRepository.save(job);
    }

    @Test
    public void getJob() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        assertThat(response.getBody(), equalTo("Greetings from Spring Boot!"));
    }
	
	
	@Test
	public void testFindJobByID() {
		 ResponseEntity<Job> response = template.getForEntity(base.toString()+"/jobs/"+ID,
                Job.class);
		 Job job_aux = response.getBody();
		 assertTrue(job_aux!=null);
    }
    
    
	@Test
	public void testFindJobByName() {
		 ResponseEntity<Job> response = template.getForEntity(base.toString()+"/jobs/"+NAME,
                Job.class);
		 Job job_aux = response.getBody();
		 assertTrue(job_aux!=null);
	}

    @Test
	public void testDeleteJob()  throws Exception {
        template.delete(base.toString()+"/jobs/"+ID, Job.class);
	}


    @Test
    public void testPersistencJob() throws Exception {

        Job job_aux = new Job();        
        job_aux.setName("Job_1");
        job_aux.setActive(true);
        
        ResponseEntity<Job> response = template.postForEntity(base.toString()+"/jobs", job_aux,
        Job.class);
        job_aux = response.getBody();
        assertTrue(response.getBody() != null);
    }
}