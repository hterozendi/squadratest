package com.mycompany.app;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Task {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    private String name;
    private Integer weight;
	private boolean completed;
	
	@Temporal(TemporalType.DATE)
	private Date createdAt;

    public Task() {}

    public Task(String name, int weight, boolean completed) {
        this.name = name;
        this.weight = weight;
		this.completed = completed;
        this.createdAt = new Date();
    }

    @Override
    public String toString() {
        return String.format(
                "Task[id=%d, name='%s', weight='%s']",
                id, name, weight);
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public boolean isCompleted() {
		return completed;
	}

	public void setCompleted(boolean completed) {
		this.completed = completed;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

}