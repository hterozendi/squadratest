package com.mycompany.app;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tasks")
public class TaskController {	
	
	@Autowired
	private TaskRepository taskRepository;
	

	
	@PostMapping
	public Task createTask(@RequestBody Task task){
		task = taskRepository.save(task);
		return task;
	}
	
	@GetMapping
	public Iterable<Task> getTask(@RequestParam("name")String name){
		if(name!=null){
			return taskRepository.findByName(name);
		}else{
			return taskRepository.findAll();
		}
	}
	
	@GetMapping("{id}")
	public Task getTask(@PathVariable Long id){
		
		return taskRepository.findById(id).get();
	}
	
	@DeleteMapping("{id}")
	public void deleteTask(@PathVariable Long id){
		
		taskRepository.deleteById(id);
	}
	
	@PutMapping("{id}")
	public Task putJob(@PathVariable Long id, @RequestBody Task task){
		Task taskAux = taskRepository.findById(id).get();
		taskAux.setName(task.getName());
		taskAux.setWeight(task.getWeight());
		taskAux.setCompleted(task.isCompleted());
		taskAux.setCreatedAt(task.getCreatedAt());
		return taskRepository.save(taskAux);
		
	}
}