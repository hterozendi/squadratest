package com.mycompany.app;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.List;

import javax.persistence.*;

@Entity
public class Job {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
	
	@Column(unique=true)
    private String name;
	private boolean active;
	
	@OneToMany(cascade=CascadeType.ALL)
	private List<Task> tasks; 
	
	@ManyToOne
	private Job parentJob;

    public Job() {}

    public Job(String name, boolean active) {
        this.name = name;
		this.active = active;
    }

    @Override
    public String toString() {
        return String.format(
                "Job[id=%d, name='%s', active='%s']",
                id, name, active);
    }
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}
	
	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	public Job getParentJob() {
		return parentJob;
	}

	public void setParentJob(Job parentJob) throws Exception{
		//if(parentJob.getId() < getId()){
			this.parentJob = parentJob;
		//}else{
		//	throw new Exception("Hierarquia não permitida!");
		//}
	}



}