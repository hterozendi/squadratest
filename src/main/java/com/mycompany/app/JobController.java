package com.mycompany.app;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.*;
import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class JobController {

	@Autowired
	private JobRepository jobRepository;
	
    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }
	
	@PostMapping("/jobs")
	public Job createJob(@RequestBody Job job) throws Exception{

		if(job.getParentJob() != null){
			if(jobRepository.existsById(job.getParentJob().getId())){				
				job = jobRepository.save(job);
			}else{
				throw new Exception("parentJob inexistente!");
			}
		}else{
			job = jobRepository.save(job);
		}
		
		return job;
	}
	
	@GetMapping("/jobs")
	public Iterable<Job> getJob(@RequestParam("name")String name){
		if(name!=null){
			return jobRepository.findByName(name);
		}else{
			return jobRepository.findAll();
		}
	}
	
	@GetMapping("/jobs/{id}")
	public Job getJob(@PathVariable Long id){
		
		return jobRepository.findById(id).get();
	}
	
	
	@DeleteMapping("/jobs/{id}")
	public void deleteJob(@PathVariable Long id) throws Exception{		
		jobRepository.deleteById(id);
	}
	
	@PutMapping("/jobs/{id}")
	public Job putJob(@PathVariable Long id, @RequestBody Job job) throws Exception{
		if(job.getParentJob() != null){
			if(!jobRepository.existsById(job.getParentJob().getId())){				
				throw new Exception("parentJob inexistente!");
			} else if(job.getParentJob().getId() >= id){	
				throw new Exception("Hierarquia inválida!");
			}
		}
		Job jobAux = jobRepository.findById(id).get();
		jobAux.setName(job.getName());
		jobAux.setActive(job.isActive());
		jobAux.getTasks().addAll(job.getTasks());
		jobAux.setParentJob(job.getParentJob());
		return jobRepository.save(jobAux);
		
	}


}